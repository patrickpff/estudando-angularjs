import { Hero } from './hero';

export const HEROES: Hero[] = [
	{id: 1, name: 'Goku'},
	{id: 2, name: 'Saitama'},
	{id: 3, name: 'Dr. Manhatan'},
	{id: 4, name: 'Batman'},
	{id: 5, name: 'Superman'},
	{id: 6, name: 'Spiderman'},
	{id: 7, name: 'Tempestade'},
	{id: 8, name: 'Prof. Xavier'},
	{id: 9, name: 'Wolverine'},
	{id: 10, name: 'Ciclope'},
	{id: 11, name: 'Mutano'},
	{id: 12, name: 'Robin'},
]